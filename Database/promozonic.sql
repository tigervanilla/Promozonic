-- MySQL dump 10.11
--
-- Host: localhost    Database: promozonic
-- ------------------------------------------------------
-- Server version	5.0.45-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `entity_type`
--

DROP TABLE IF EXISTS `entity_type`;
CREATE TABLE `entity_type` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `name` varchar(15) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `entity_type`
--

LOCK TABLES `entity_type` WRITE;
/*!40000 ALTER TABLE `entity_type` DISABLE KEYS */;
INSERT INTO `entity_type` VALUES (1,'2018-06-19 10:22:45','image'),(2,'2018-06-19 10:22:45','video'),(4,'2018-06-19 10:22:45','audio');
/*!40000 ALTER TABLE `entity_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `title` varchar(255) NOT NULL,
  `Event_type_id` int(11) unsigned default NULL,
  `vendor_id` int(11) unsigned default NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `total_prizes` int(11) unsigned NOT NULL,
  `Moderator required` tinyint(1) default NULL,
  `description` text,
  `prize_description` text,
  `prize_validity` date default NULL,
  `slug` varchar(127) default NULL,
  `winner_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk5` (`Event_type_id`),
  KEY `fk6` (`vendor_id`),
  KEY `fk_prize` (`winner_id`),
  CONSTRAINT `fk_prize` FOREIGN KEY (`winner_id`) REFERENCES `prize` (`user_id`),
  CONSTRAINT `fk5` FOREIGN KEY (`Event_type_id`) REFERENCES `event_type` (`id`),
  CONSTRAINT `fk6` FOREIGN KEY (`vendor_id`) REFERENCES `vendor_profile` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'2018-06-23 06:24:08','Z',1,1,'2018-05-15','2018-05-30',10,1,'@@@','$$$','2018-08-30','@url',1),(2,'2018-06-23 06:25:22','Y',1,2,'2018-06-15','2018-06-30',10,1,'@@@','$$$','2018-08-30','@url',8),(3,'2018-06-23 06:25:22','X',1,3,'2018-07-15','2018-07-30',10,1,'@@@','$$$','2018-08-30','@url',9),(4,'2018-06-23 06:25:22','W',1,4,'2018-08-15','2018-08-30',10,1,'@@@','$$$','2018-08-30','@url',10),(5,'2018-06-23 06:25:23','y',1,2,'2018-07-25','2018-07-30',10,1,'@@@','$$$','2018-08-30','@url',3);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_type`
--

DROP TABLE IF EXISTS `event_type`;
CREATE TABLE `event_type` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `name` varchar(63) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_type`
--

LOCK TABLES `event_type` WRITE;
/*!40000 ALTER TABLE `event_type` DISABLE KEYS */;
INSERT INTO `event_type` VALUES (1,'2018-06-19 10:19:52','photo Event'),(2,'2018-06-19 10:19:52','dance'),(3,'2018-06-19 10:19:52','singing'),(4,'2018-06-19 10:19:52','video'),(5,'2018-06-19 10:19:52','survey');
/*!40000 ALTER TABLE `event_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `firm_domain`
--

DROP TABLE IF EXISTS `firm_domain`;
CREATE TABLE `firm_domain` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `name` varchar(63) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `firm_domain`
--

LOCK TABLES `firm_domain` WRITE;
/*!40000 ALTER TABLE `firm_domain` DISABLE KEYS */;
INSERT INTO `firm_domain` VALUES (1,'2018-06-21 01:02:39','garments'),(2,'2018-06-21 01:02:50','electronics'),(3,'2018-06-21 01:03:09','cosmetics'),(4,'2018-06-21 01:03:24','movies'),(5,'2018-06-21 01:03:34','groceries');
/*!40000 ALTER TABLE `firm_domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prize`
--

DROP TABLE IF EXISTS `prize`;
CREATE TABLE `prize` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `event_id` int(11) unsigned default NULL,
  `user_id` int(11) unsigned default NULL,
  `vendor_id` int(11) unsigned default NULL,
  `coupon_code` varchar(15) default NULL,
  `recieved` tinyint(1) default NULL,
  `score` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `coupon_code` (`coupon_code`),
  KEY `fk7` (`vendor_id`),
  KEY `fk8` (`user_id`),
  KEY `fk9` (`event_id`),
  CONSTRAINT `fk7` FOREIGN KEY (`vendor_id`) REFERENCES `vendor_profile` (`user_id`),
  CONSTRAINT `fk8` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk9` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prize`
--

LOCK TABLES `prize` WRITE;
/*!40000 ALTER TABLE `prize` DISABLE KEYS */;
INSERT INTO `prize` VALUES (1,'2018-06-21 02:31:11',1,1,1,'XDS@879',1,0),(2,'2018-06-21 02:31:11',2,2,2,'GDS@879',1,0),(3,'2018-06-21 02:31:11',3,3,3,'HGS@879',1,0),(4,'2018-06-21 02:31:11',4,4,4,'YTS@879',0,0),(5,'2018-06-21 05:44:49',5,3,2,'pqr@987',0,0),(6,'2018-06-21 02:31:11',1,6,1,'sdr@987',0,0),(7,'2018-06-21 02:31:11',1,7,1,'bnh@333',0,0),(8,'2018-06-21 03:31:27',2,7,2,'jkh@333',0,0),(9,'2018-06-21 03:31:27',2,8,2,'jkh@993',0,0),(10,'2018-06-21 03:31:27',3,9,3,'yui@393',0,0),(11,'2018-06-21 03:31:27',3,10,3,'dui@393',0,0),(12,'2018-06-21 03:31:27',4,10,4,'fdi@393',0,0),(13,'2018-06-21 03:31:27',4,5,4,'mji@393',0,0),(14,'2018-06-21 05:44:49',5,3,2,'fvi@393',0,0);
/*!40000 ALTER TABLE `prize` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `name` varchar(15) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'2018-06-21 00:52:55','end_user'),(2,'2018-06-21 00:53:25','vendor'),(3,'2018-06-21 00:53:34','admin');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `first_name` varchar(15) NOT NULL,
  `last_name` varchar(15) NOT NULL,
  `email` varchar(63) NOT NULL,
  `mobile_no` varchar(15) NOT NULL,
  `dob` date default NULL,
  `gender` char(1) NOT NULL,
  `house_no` varchar(15) default NULL,
  `street` varchar(31) default NULL,
  `city` varchar(15) NOT NULL,
  `locality` varchar(31) NOT NULL,
  `state` varchar(31) default NULL,
  `pincode` varchar(6) default NULL,
  `active` tinyint(1) default NULL,
  `img` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_email` (`email`),
  UNIQUE KEY `unique_mobile` (`mobile_no`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2018-06-19 10:16:17','ABC','verma','abc@gmail.com','987654321','1998-08-15','M','146','sector-4','mathura','@@@','U.P','281001',0,'user_picture'),(2,'2018-06-19 10:16:17','PQR','sharma','pqr@gmail.com','987654300','1999-02-27','F','148','sector-4','mathura','@@@','U.P','281001',1,'user_picture'),(3,'2018-06-19 10:16:17','UVW','sharma','uvw@gmail.com','987654302','1997-06-05','F','149','sector-4','mathura','@@@','U.P','281001',1,'user_picture'),(4,'2018-06-19 10:16:17','XYZ','yadav','xyz@gmail.com','987654329','1997-09-08','F','150','sector-4','mathura','@@@','U.P','281001',1,'user_picture'),(5,'2018-06-21 02:15:19','raju','verma','imraju@gmail.com','8844556622','1998-08-15','M','146','sector-4','mathura','@@@','U.P','281001',0,'user_picture'),(6,'2018-06-21 02:15:29','raju','verma','shrivastav@gmail.com','8844556623','1998-08-15','M','146','sector-4','mathura','@@@','U.P','281001',0,'user_picture'),(7,'2018-06-21 02:15:40','raju','gadha','vastav@gmail.com','8844556633','1998-08-15','M','146','sector-4','mathura','@@@','U.P','281001',0,'user_picture'),(8,'2018-06-21 02:15:47','piyush','sharma','vastav@yahoo.com','8844556634','1998-08-15','M','146','sector-4','mathura','@@@','U.P','281001',0,'user_picture'),(9,'2018-06-21 02:15:55','pp','singh','ps@gmail.com','8844556635','1998-08-15','M','146','sector-4','mathura','@@@','U.P','281001',0,'user_picture'),(10,'2018-06-21 02:16:03','pqr','singh','ppsingh@gmail.com','8844556636','1998-08-15','F','146','sector-4','mathura','@@@','U.P','281001',0,'user_picture');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_entity_like`
--

DROP TABLE IF EXISTS `user_entity_like`;
CREATE TABLE `user_entity_like` (
  `ID` int(11) unsigned NOT NULL auto_increment,
  `user_id` int(11) unsigned default NULL,
  `user_entity_upload_id` int(11) unsigned default NULL,
  `CREATION_TIMESTAMP` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `unique_like` (`user_id`,`user_entity_upload_id`),
  KEY `fk14` (`user_entity_upload_id`),
  CONSTRAINT `fk13` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk14` FOREIGN KEY (`user_entity_upload_id`) REFERENCES `user_entity_upload` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_entity_like`
--

LOCK TABLES `user_entity_like` WRITE;
/*!40000 ALTER TABLE `user_entity_like` DISABLE KEYS */;
INSERT INTO `user_entity_like` VALUES (1,1,1,'2018-06-21 04:11:13'),(2,2,1,'2018-06-21 04:11:29'),(3,3,1,'2018-06-21 04:11:32'),(4,5,1,'2018-06-21 04:11:50'),(5,4,1,'2018-06-21 04:11:53'),(6,4,2,'2018-06-21 04:13:56'),(7,2,2,'2018-06-21 04:14:00'),(8,3,3,'2018-06-21 04:14:32'),(9,8,3,'2018-06-21 04:14:37'),(10,1,3,'2018-06-21 04:14:41'),(11,4,4,'2018-06-21 04:15:08'),(12,4,5,'2018-06-21 04:15:13'),(13,5,5,'2018-06-21 04:15:15'),(14,1,6,'2018-06-21 04:17:19'),(15,2,6,'2018-06-21 04:17:25'),(16,3,6,'2018-06-21 04:17:28'),(17,4,6,'2018-06-21 04:17:31'),(18,5,6,'2018-06-21 04:17:35'),(19,6,6,'2018-06-21 04:17:39'),(20,6,7,'2018-06-21 04:19:37'),(21,7,7,'2018-06-21 04:19:40'),(22,9,7,'2018-06-21 04:19:43'),(23,2,8,'2018-06-21 04:19:54'),(24,4,8,'2018-06-21 04:19:58'),(25,6,8,'2018-06-21 04:20:05'),(26,8,8,'2018-06-21 04:20:08'),(27,3,9,'2018-06-21 04:20:13'),(28,6,9,'2018-06-21 04:20:16'),(29,9,9,'2018-06-21 04:20:18'),(30,10,10,'2018-06-21 04:20:27'),(31,9,10,'2018-06-21 04:20:32'),(32,7,10,'2018-06-21 04:20:36'),(33,5,10,'2018-06-21 04:20:39'),(34,3,10,'2018-06-21 04:20:42'),(35,1,10,'2018-06-21 04:20:45'),(36,1,11,'2018-06-21 04:20:53'),(37,5,11,'2018-06-21 04:20:58'),(38,10,11,'2018-06-21 04:21:07'),(39,6,12,'2018-06-21 04:21:43'),(40,3,13,'2018-06-21 04:21:48');
/*!40000 ALTER TABLE `user_entity_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_entity_upload`
--

DROP TABLE IF EXISTS `user_entity_upload`;
CREATE TABLE `user_entity_upload` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `entity_type_id` int(11) unsigned default NULL,
  `user_id` int(11) unsigned default NULL,
  `event_id` int(11) unsigned default NULL,
  `entity_file_url` varchar(127) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk10` (`entity_type_id`),
  KEY `fk11` (`event_id`),
  KEY `fk12` (`user_id`),
  CONSTRAINT `fk10` FOREIGN KEY (`entity_type_id`) REFERENCES `entity_type` (`id`),
  CONSTRAINT `fk11` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `fk12` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_entity_upload`
--

LOCK TABLES `user_entity_upload` WRITE;
/*!40000 ALTER TABLE `user_entity_upload` DISABLE KEYS */;
INSERT INTO `user_entity_upload` VALUES (1,'2018-06-21 02:40:08',1,1,1,'img@img.com'),(2,'2018-06-21 02:40:22',1,6,1,'img6@img.com'),(3,'2018-06-21 02:40:29',1,7,1,'img7@img.com'),(4,'2018-06-21 03:26:22',1,7,2,'img27@img.com'),(5,'2018-06-21 03:26:37',1,2,2,'img22@img.com'),(6,'2018-06-21 03:26:45',1,8,2,'img28@img.com'),(7,'2018-06-21 03:27:57',1,3,3,'img33@img.com'),(8,'2018-06-21 03:28:04',1,9,3,'img39@img.com'),(9,'2018-06-21 03:28:18',1,10,3,'img310@img.com'),(10,'2018-06-21 03:42:52',1,10,4,'img410@img.com'),(11,'2018-06-21 03:43:12',1,5,4,'img45@img.com'),(12,'2018-06-21 03:43:23',1,6,5,'img56@img.com'),(13,'2018-06-21 03:44:11',1,3,5,'img53@img.com');
/*!40000 ALTER TABLE `user_entity_upload` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `role_id` int(11) unsigned default NULL,
  `user_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk1` (`role_id`),
  KEY `fk2` (`user_id`),
  CONSTRAINT `fk1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `fk2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,'2018-06-21 00:57:46',2,1),(2,'2018-06-21 00:57:46',2,2),(3,'2018-06-21 00:57:46',2,3),(5,'2018-06-21 00:57:46',2,4),(6,'2018-06-21 00:57:26',1,5),(7,'2018-06-21 00:56:01',1,6),(8,'2018-06-21 00:56:05',1,7),(9,'2018-06-21 00:56:08',1,8),(10,'2018-06-21 00:56:12',1,9),(11,'2018-06-21 00:56:16',1,10);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor_profile`
--

DROP TABLE IF EXISTS `vendor_profile`;
CREATE TABLE `vendor_profile` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `creation_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `name` varchar(255) NOT NULL,
  `street` varchar(31) NOT NULL,
  `city` varchar(15) NOT NULL,
  `locality` varchar(31) NOT NULL,
  `domain_id` int(11) unsigned default NULL,
  `user_id` int(11) unsigned default NULL,
  `shopNo` varchar(15) NOT NULL,
  `gstn` varchar(15) default NULL,
  `img` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk3` (`domain_id`),
  KEY `fk4` (`user_id`),
  CONSTRAINT `fk3` FOREIGN KEY (`domain_id`) REFERENCES `firm_domain` (`id`),
  CONSTRAINT `fk4` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor_profile`
--

LOCK TABLES `vendor_profile` WRITE;
/*!40000 ALTER TABLE `vendor_profile` DISABLE KEYS */;
INSERT INTO `vendor_profile` VALUES (1,'2018-06-20 03:25:24','A','sector-5','mathura','@@@',1,1,'150','11111','vendor@image.com'),(2,'2018-06-20 03:25:24','B','sector-6','mathura','@@@',2,2,'151','22222','vendor@image.com'),(3,'2018-06-20 03:25:24','C','sector-7','mathura','@@@',3,3,'152','33333','vendor@image.com'),(4,'2018-06-20 03:25:24','D','sector-8','mathura','@@@',4,4,'153','44444','vendor@image.com');
/*!40000 ALTER TABLE `vendor_profile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-23  6:44:19
