var sessionUtils = require('../utils/sessionUtils');
var util = require('util');
var databaseUtils = require('./../utils/databaseUtils');

module.exports = {
    showHomePage: function* (next) {

        var userId=this.request.query.id;
        var showemail=this.request.query.showemail;
        var queryString = 'select * from user where id="%s"';
        var query = util.format(queryString, userId);
        var result = yield databaseUtils.executeQuery(query);
        var userDetails = result[0];
        yield this.render('home',{
            showEmail:showemail === 'true',
            userDetails:userDetails
        });
    },

    logout: function* (next) {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            sessionUtils.deleteSession(sessionId);
        }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});

        this.redirect('/');
    },

    showTest: function* (next) {
        yield this.render('test',{

        });
    },

    showGLA
    
    : function* (next) {
        yield this.render('gla',{

        });
    }
}
